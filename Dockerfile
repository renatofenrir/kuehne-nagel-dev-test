FROM openjdk:10-jre-slim
EXPOSE 8080
COPY build/libs/*SNAPSHOT-fat.jar /app/ 
WORKDIR /app
CMD java -jar -Dvertx.disableFileCPResolving=true *SNAPSHOT-fat.jar 
USER root
