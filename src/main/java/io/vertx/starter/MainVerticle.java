package io.vertx.starter;

import io.vertx.core.AbstractVerticle;
import java.util.Map;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.buffer.Buffer;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;


public class MainVerticle extends AbstractVerticle {

  @Override
  public void start() {

    String pod_name = System.getenv("HOSTNAME");

    vertx.createHttpServer()
        .requestHandler(req -> req.response().end(String.format("<h1>TO VICTORY!! Hello from pod ==> %s</h1>", pod_name)))
        .listen(8080);
  

  // will now try to fetch those URLs :)
  WebClient client = WebClient.create(vertx);

  client.get(80, "www.google.com", "/").send(ar -> {
    if (ar.succeeded()) {
      HttpResponse<Buffer> response = ar.result();
      System.out.println("Got HTTP response with status " + response.statusCode());
    } else {
      ar.cause().printStackTrace();
    }
  });

  }
}
