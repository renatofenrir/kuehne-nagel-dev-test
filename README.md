# Kuehne-Nagel Developer Test by Renato Araujo

This little project is intended to show you a demo of my DevOps habilities.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

- Docker installation on local machine or cloud provider;
- Jenkins installation with nodes with docker installed (because they are going to be executing all docker build workloads);
- Raw kubernetes cluster / Red Hat Openshift.


### First things first: Jenkins builds

In order to run this application, we must execute a Jenkins build pointing to the provided Jenkinsfile. 

To do that, create a new job in Jenkins of type **Pipeline** and then while accessing the configuration page, on the 
section 'Pipeline', select the option **Pipeline Script from SCM**, put the address of the repository and then click Ok. 

Note: To be able to access external SCM service, credentials configuration may be required.

This job has the following structure:

1. **Source-Code Checkout** (Here Jenkins is going to be downloading the project's source-code to its workplace);
2. **Source-Code Compilation** (Here Jenkins is going to use Gradle to compile the project);
3. **Docker image build/push** (Here the Jenkins node that is going to execute this workload will docker build the image an then 
push to a image registry - In my case I have just configured my Docker Hub account but this structure allows the user to configure
other registry services);
4. And finally, once pushed, the Jenkins node is going to **remove the image from its local cache** for storage saving purposes. :)


### Docker images involved in this scenario

In this scenario I'll be using the following images:

1. **gradle:jdk10** to execute the gradle build;
2. Application minimal image: **renatofenrir/dummy-vertx-test:v1** 

## To execute the image locally:

```
 docker run -ti -d -p 8080:8080 renatofenrir/dummy-vertx-test:v1
```

And then to check if is running:

```
 curl localhost:8080
```

Can also be checked by executing the following:

```
 netstat -atunp |grep 8080
```
And the if the following output is shown in the shell, it means that the image is up and running:

```
 tcp6       0      0 :::8080                 :::*                    LISTEN       25499/docker-proxy
```

## To execute in a stock Kubernetes cluster:

To do that it is necessary to have a cluster up and running. If it happens of not having 
this environment available, I sugest accessing the **Play With K8s** playground environment 
that consists of a service where it is possible to deploy instances to spin-up a fresh kubernetes
cluster. To get access to this resource:

1. Access https://labs.play-with-k8s.com/ ;
2. Provide Github or Docker hub credentials ;
3. Deploy instances (they only allow up to 5) ;
4. Bootstrap the cluster following the provided instructions ;
5. Execute the bellow commands, in order to start the application using the provided yaml files:


After deploying nodes, check if they are already up:

```
kubectl get nodes
```

If so, proceed with the following:

```
 cd k8s-setup
 kubectl apply -f dummy-vertx-test-deployment.yaml
```

And now, to check if everything is up and running:

```
 kubectl get pods
```

An output similar to this one is going to be shown:

```
NAME                                READY     STATUS    RESTARTS   AGE
dummy-vertx-test-57448d7588-4l9tp   1/1       Running   0          32s
dummy-vertx-test-57448d7588-w2lnf   1/1       Running   0          32s
```

After getting the pods up and running, now it is time to expose them with a 
service. To do that execute the following:

```
 cd k8s-setup
 kubectl apply -f dummy-vertx-test-service.yaml
```

And then, check if the service is already up by running:

```
kubectl get svc
```

An output similar to this one is going to be shown:

```
NAME               TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
dummy-vertx-test   NodePort    10.101.118.32   <none>        8080:32494/TCP   4m
```

Now that we know that the service is already up, we can describe its configuration,
just to make sure everything went fine by executing the following command:

```
kubectl describe svc dummy-vertx-test
```


An output similar to this one is going to be displayed:

```
Name:                     dummy-vertx-test
Namespace:                default
Labels:                   <none>
Annotations:              kubectl.kubernetes.io/last-applied-configuration={"apiVersion":"v1","kind":"Service","metadata":{"annotations":{},"name":"dummy-vertx-tes
t","namespace":"default"},"spec":{"ports":[{"name":"http","port...
Selector:                 app=dummy-vertx-test
Type:                     NodePort
IP:                       10.101.118.32
Port:                     http  8080/TCP
TargetPort:               8080/TCP
NodePort:                 http  32494/TCP
Endpoints:                10.32.0.3:8080,10.46.0.1:8080
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>
```


And now we know that the service is using the NodePort **http  32494/TCP**. After exposing the pods with this method, 
we can access the application by entering the following address in the browser:

```
http://<node_ip>:32494
```

It is also possible to check if the URL is accessible by using cURL:

```
curl http://localhost:32494
```

And a similar output is going to be shown:

```
[node1 ~]$ curl http://localhost:32494
<h1>Hello from foobar! Hello from pod ==> dummy-vertx-test-57448d7588-4l9tp</h1>[node1 ~]$
```

Which means that the URL is accessible, the pods are up and running and the service is properly exposing them via **NodePort**.


## To execute in a RedHat OpenShift cluster:


To achieve this, it is possible to execute the application with two different approaches:

1. By deploying the image directly to Openshift;
2. By applying the provided yaml files (deployment and service) and then creating a route 
to expose the pods.


# Deploying the image directly to Openshift:

1. Create a project with a name of your choice;
2. Once created, get to the overview page and then click **Deploy Image**;
3. Select the option 'Image Name', so openshift can search for it on public registry, in this case, **Docker Hub**;
4. Paste **renatofenrir/dummy-vertx-test:v1** as image name in the input box;
5. Click on the magnifying glass icon, and then OpenShift is going search for the image;
6. Once found, let all settings as is and then click **Deploy**;
7. Once deployed, wait for the pod to spin up, and then create a route. To do that, go to the **Applications** section,
and then click on **Routes**. OpenShift is going to display a message, saying that there are no services created yet so
go ahead and click on the **Create Route** button;
8. Give a name to the route and then click the **Create** button;
9. Now wait for the route to be available. This often take around 3 minutes, approximately.
10. Click on the URL crated for this route. It would be something like the following:

```
http://dummy-vertx-app-route-dummy-vertx-app.193b.starter-ca-central-1.openshiftapps.com
```

OpenShift is going to redirect to the application. If everything went fine, it is going to 
be shown in your browser the following:

```
Hello from foobar! Hello from pod ==> dummy-vertx-test-1-rwvq5
```


# Deploying the yaml files to Openshift:

1. Create a project with a name of your choice;
2. Once created, get to the overview page and then click **Import YAML / JSON**;
3. You can copy and paste or upload the files. Firstly, upload or paste the file
**dummy-vertx-test-deployment.yaml** so the deployment can be started right away;
4. Once that is done, click on the button **Add to Project** located in the top
right corner, near the **Search Catalog** bar and then, select the option **Import YAML / JSON**
afterwards;
5. Upload or Copy and Paste the file **dummy-vertx-test-service.yaml** so now, the pods
are going to be exposed with this service, so they can be accessed externally;
6. Once deployed, wait for the pod to spin up, and then create a route. To do that, go to the **Applications** section,
and then click on **Routes**. OpenShift is going to display a message, saying that there are no routes created yet so
go ahead and click on the **Create Route** button;
7. Give a name to the route and then click the **Create** button;
8. Now wait for the route to be available. This often take around 3 minutes, approximately.
9. Click on the URL crated for this route. It would be something like the following:


```
http://dummy-vertx-app-route-dummy-vertx-app.193b.starter-ca-central-1.openshiftapps.com
```

OpenShift is going to redirect to the application. If everything went fine, it is going to 
be shown in your browser the following:

```
Hello from foobar! Hello from pod ==> dummy-vertx-test-1-rwvq5
```